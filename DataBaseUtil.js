/**
 * Created by RV on 20/03/17.
 */

var User = require('./UserModel');
var obdData = require('./OBDResponse');

module.exports = {
    getUserByemail : function(email,callback) {
        User.findOne({'email' : email},function(err,user) {
            if (err) {
                callback(err);
            }else {
                callback(null,user);
            }
        });
    },

    getUserByToken : function(token,callback) {
        User.findOne({'token' : token},function(err,user) {
            if (err) {
                callback(err);
            }else {
                callback(null,user);
            }
        });
    },

    getObdDataByDate : function (startDate,endDate,commandType,callback) {
        console.log(startDate +" "+endDate);
        obdData.find({'name':commandType,
            'entryDate':{
            '$gte':startDate, '$lte':endDate
        }
        },function(err,result){
            if (err){
                callback(err,null);
            }else {
                callback(null,result);
            }
        });
    }

};
