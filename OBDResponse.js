/**
 * Created by RV on 17/02/17.
 */

var mongoose = require('mongoose');

var obdData = mongoose.Schema({
    entryDate : Date,
    name : String,
    value : Number
});

var obdDataModel =  mongoose.model('ObdData', obdData);

module.exports = obdDataModel;