/**
 * Created by RV on 05.06.17.
 */
var express = require('express');
var router = express.Router();
var dataUtil = require("../DataBaseUtil");

router.post('/',function(req,res,next) {

    dataUtil.getObdDataByDate(req.body.startDate,req.body.endDate,req.body.commandType,function(err,result){
        res.send({'datas':result})

    })
});

module.exports = router;