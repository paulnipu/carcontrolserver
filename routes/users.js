var express = require('express');
var User = require('../UserModel');
var uuid = require('node-uuid');
var router = express.Router();
var dataUtil = require("../DataBaseUtil");

/* GET users listing. */
router.post('/signup', function(req, res, next) {
  var newUser = new User(req.body);
  dataUtil.getUserByemail(newUser.email,function(error,user) {
    if (user == null){
      var tokenResponse = uuid.v1();
      newUser.token = tokenResponse;
      newUser.save(function (error,resource){
        if (error){
          next(error)
        }else {
          console.log("User Saved "+newUser.name);
          var tokens = {
            token : tokenResponse
          };
          res.send(tokens);
        }
      });
    }else {
      res.sendStatus(401);
    }
  });

});

router.post('/login', function(req, res, next) {
  dataUtil.getUserByemail(req.body.email,function(error,user) {
    if (error) {
      next(error);
    }else if (user){
      if (user.password == req.body.password) {
        res.send(user)
      }else {
        res.sendStatus(401)
      }

    }else {
      res.sendStatus(401);
    }
  });
});

module.exports = router;
