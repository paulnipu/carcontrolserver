/**
 * Created by RV on 19/03/17.
 */
var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    name : String,
    email : String,
    password : String,
    carNumber : String,
    token : String
});

var User = mongoose.model('User', userSchema);

module.exports = User;
